import Vue from 'vue';
// 按需导入
import 
{ Button, 
    Form, 
    FormItem, 
    Input, 
    Message,
    Container,
    Header,
    Aside,
    Main,
    Menu,
    Submenu,
    MenuItem, 
} 
from 'element-ui'

// 使用vue.use() 注册为全局组件
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItem)
// message组件有点不一样 需要全局挂载
Vue.prototype.$message = Message
