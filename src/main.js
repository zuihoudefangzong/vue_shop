import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入elementui的css
import 'element-ui/lib/theme-chalk/index.css';
import './plugins/element.js'
// 导入全局样式
import './assets/css/global.css';
// 导入字体图标
import './assets/fonts/iconfont.css';
// 导入axios包
import axios from 'axios';
// 那么每个this 都能访问$http
Vue.prototype.$http=axios;
// 设置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/';
// axios请求request拦截器
// 后台除了登录接口之外，
// 都需要token权限验证，我们可以通过添加axios请求拦截器来添加token
// 需要授权的 API ，必须在请求头中使用Authorization字段提供token令牌
axios.interceptors.request.use(config=>{
  // console.log(config);
  config.headers.Authorization =window.sessionStorage.getItem('token');
  // must return confing
  return config
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
