import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
Vue.use(VueRouter)

const routes = [
  // redirect重定向到/login
  {
    path:'/',redirect:'/login'
  },
  {
    path:'/login',
    component:Login,
  },
  {

    path:'/home',
    component:Home,
    // redirect重定向到/home/welcome
    redirect:'/welcome',
    children:[{
      // 这时候/home就像一个根组件
      // 利用了路由占位  再访问/welcome
      path:'/welcome',
      component:Welcome,
    }]
  },
];
const router = new VueRouter({
  routes
})
// 路由导航守卫 为router添加beforeEach导航守卫
router.beforeEach((to,from,next)=>{
  // 如访问的是/login 直接放行
  if(to.path=='/login') return next();
  // 从会话sessionStorage获取getItem到token的值
  const tokenStr=window.sessionStorage.getItem('token');
  // 若没有获取到 就是没登录 直接跳转到/login
  if(!tokenStr) return next('/login');
  // 最后上面都没成立 就是登录过 但访问的是路径 直接放行
  next();
})

export default router
